import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import data from '../../../../public/data.json';
import {
  fetchData as fetchDataAction,
  filterCars as filterCarsAction,
  updateCheckboxes as updateCheckboxesAction
} from '../modules';
import { CarCards, CarCheckboxes, FilterInput } from '../ui';

class Container extends React.Component {
  static propTypes = {
    fetchData: PropTypes.func.isRequired,
    filterCars: PropTypes.func.isRequired,
    updateCheckboxes: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      cars: [],
      filteredCars: [],
      filtering: false
    };
  }

  componentDidMount() {
    const { fetchData } = this.props;

    fetchData(data);
  }

  static getDerivedStateFromProps(nextProps) {
    const { cars, filteredCars } = nextProps;

    return { cars, filteredCars };
  }

  onButtonClick = (value) => {
    const { filterCars } = this.props;

    if (value.length > 0) {
      this.setState({ filtering: true });
      filterCars(value);
    } else {
      this.setState({ filtering: false });
    }
  }

  render() {
    const { updateCheckboxes } = this.props;
    const {
      cars,
      filteredCars,
      filtering
    } = this.state;

    return (
      <div>
        <FilterInput
            handleButtonClick={this.onButtonClick}
        />
        <CarCards
            cars={cars}
            filteredCars={filteredCars}
            filtering={filtering}
        />
        <CarCheckboxes
            cars={cars}
            handleCheck={updateCheckboxes}
        />
      </div>
    );
  }
}

const mapDispatchToProps = {
  fetchData: fetchDataAction,
  filterCars: filterCarsAction,
  updateCheckboxes: updateCheckboxesAction
};

const mapStateToProps = ({ home }) => ({
  cars: home.data.cars,
  filteredCars: home.filteredCars
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);
