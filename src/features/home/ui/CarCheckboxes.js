import PropTypes from 'prop-types';
import React from 'react';
import { Checkbox } from 'semantic-ui-react';

import '../../../App.css';

const CarCheckboxes = ({ cars, handleCheck }) => (
  <div className="CarCheckboxes">
    {cars.map(car => (
      <Checkbox
          className="CarCheckbox"
          key={car.name}
          label={car.name}
          onChange={() => handleCheck(car.name)}
      />
    ))}
  </div>
);

CarCheckboxes.propTypes = {
  cars: PropTypes.arrayOf(
    PropTypes.shape({
      checked: PropTypes.bool,
      name: PropTypes.string
    })
  ).isRequired,
  handleCheck: PropTypes.func.isRequired
};

export default CarCheckboxes;
