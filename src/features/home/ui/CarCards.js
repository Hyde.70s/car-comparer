import PropTypes from 'prop-types';
import React from 'react';
import { Grid } from 'semantic-ui-react';

import '../../../App.css';

const RenderCarCards = ({ renderedCars }) => (
  <Grid
      celled="internally"
      columns="equal"
  >
    <Grid.Row>
      {renderedCars.filter(car => car.id < 4).map(car => (
        <Grid.Column
            id={`Car${car.id}`}
            key={car.id}
        >
          <div className="CarCard">
            <img
                alt={car.image}
                className="CarCardImage"
                height="100"
                src={car.image}
                width="150"
            />
            <div className="CarCardName">{car.name}</div>
          </div>
        </Grid.Column>
      ))}
    </Grid.Row>
    <Grid.Row>
      {renderedCars.filter(car => car.id > 3).map(car => (
        <Grid.Column
            id={`Car${car.id}`}
            key={car.id}
        >
          <div className="CarCard">
            <img
                alt={car.image}
                className="CarCardImage"
                height="100"
                src={car.image}
                width="150"
            />
            <div className="CarCardName">{car.name}</div>
          </div>
        </Grid.Column>
      ))}
      <Grid.Column />
    </Grid.Row>
  </Grid>
);

const CarCards = ({ cars, filteredCars, filtering }) => (
  <div>
    {filtering ?
      <RenderCarCards renderedCars={filteredCars} />
    :
      <RenderCarCards renderedCars={cars} />
    }
  </div>
);

CarCards.defaultProps = {
  filteredCars: []
};

CarCards.propTypes = {
  cars: PropTypes.arrayOf(
    PropTypes.shape({
      description: PropTypes.string,
      id: PropTypes.number,
      image: PropTypes.string,
      name: PropTypes.string,
      speed: PropTypes.number
    })
  ).isRequired,
  filteredCars: PropTypes.arrayOf(
    PropTypes.shape({
      description: PropTypes.string,
      id: PropTypes.number,
      image: PropTypes.string,
      name: PropTypes.string,
      speed: PropTypes.number
    })
  ),
  filtering: PropTypes.bool.isRequired
};

RenderCarCards.propTypes = {
  renderedCars: PropTypes.arrayOf(
    PropTypes.shape({
      description: PropTypes.string,
      id: PropTypes.number,
      image: PropTypes.string,
      name: PropTypes.string,
      speed: PropTypes.number
    })
  ).isRequired
};

export default CarCards;
