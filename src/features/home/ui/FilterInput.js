import PropTypes from 'prop-types';
import React from 'react';
import { Input } from 'semantic-ui-react';

class FilterInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: ''
    };
  }

  onInputChange = (value) => {
    this.setState({ inputValue: value });
  }

  render() {
    const { handleButtonClick } = this.props;
    const { inputValue } = this.state;

    return (
      <Input
          action={{
            icon: 'search',
            onClick: () => handleButtonClick(inputValue)
          }}
          className="FilterInput"
          fluid
          onChange={(event, data) => this.onInputChange(data.value)}
      />
    );
  }
}

FilterInput.propTypes = {
  handleButtonClick: PropTypes.func.isRequired
};

export default FilterInput;
