export const FETCH_DATA = '@home/FETCH_DATA';
export const FILTER_CARS = '@home/FILTER_CARS';
export const TEST_REDUX = '@home/TEST_REDUX';
export const UPDATE_CHECKBOXES = '@home/UPDATE_CHECKBOXES';

export function fetchData(data) {
  return {
    type: FETCH_DATA,
    payload: {
      data
    }
  };
}

export function filterCars(query) {
  return {
    type: FILTER_CARS,
    payload: {
      query
    }
  };
}

export function testRedux(testData) {
  return {
    type: TEST_REDUX,
    payload: {
      testData
    }
  };
}

export function updateCheckboxes(car) {
  return {
    type: UPDATE_CHECKBOXES,
    payload: {
      car
    }
  };
}
