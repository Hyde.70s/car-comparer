import * as actions from '../actions';

describe('Home actions', () => {
  it('should create an action TEST_REDUX', () => {
    const testData = 'testData';
    const expectedAction = {
      type: actions.TEST_REDUX,
      payload: {
        testData
      }
    };
    expect(actions.testRedux(testData)).toEqual(expectedAction);
  });
});
