import * as actions from '../actions';
import reducer from '../reducer';

describe('Home reducer', () => {
  it('should handle TEST_REDUX action', () => {
    const action = {
      type: actions.TEST_REDUX,
      payload: {
        testData: 'testData'
      }
    };

    const newState = reducer(undefined, action);

    expect(newState.testData).toEqual('testData');
  });
});
