import { FETCH_DATA, FILTER_CARS, TEST_REDUX, UPDATE_CHECKBOXES } from './actions';

const initialState = {
  checkedCars: [],
  data: {
    cars: []
  },
  filteredCars: [],
  testData: ''
};

export default function reducer(state = initialState, action) {
  const { payload } = action;

  switch (action.type) {
    case FETCH_DATA: {
      return {
        ...state,
        data: payload.data
      };
    }
    case FILTER_CARS:
      return {
        ...state,
        filteredCars: state.data.cars.filter(car => car.name.toLowerCase().includes(payload.query.toLowerCase()))
      };
    case TEST_REDUX:
      return {
        ...state,
        testData: payload.testData
      };
    case UPDATE_CHECKBOXES:
      return {
        ...state,
        checkedCars: state.checkedCars.find(car => car === payload.car)
          ? state.checkedCars.filter(car => car !== payload.car)
          : [...state.checkedCars, payload.car]
      };
    default:
      return state;
  }
}
